<h1 align="center"> 
     BSPWM
</h1>

<a href="https://imgur.com/8HxEcj7"><img src="https://imgur.com/8HxEcj7.png" title="source: imgur.com" /></a>

<h2>Dotfiles for EndeavourOS</h2>

<ul>
  <li>BSPWM</li>
  <li>POLYBAR</li>
  <li>ZSHELL + POWERLEVEL10K</li>
  <li>ALACRITTY TERMINAL</li>
  <li>SXHKD</li>
  <li>NeoVim</li>
  <li>Lunarvim</li>
</ul> 

<h2>Essencial Packages</h2>

<ul>
  <li>Yay</li>
  <li>Brillo</li>
  <li>Thunar</li>
  <li></li>
</ul> 
